#!/bin/bash

_PODGRIM_CONFIG_DIR_NAME=".podgrim"
_PODGRIM_HOSTNAME_DOMAIN="podgrim.example.com"

## Helper functions
function _PODGRIM_FUNC_project_dir {
    local current="$PWD"

    if [ -n "$_PODGRIM_VAR_project_dir" ]; then
        echo "$_PODGRIM_VAR_project_dir"
        return
    fi

    while [ "$current" != "/" ]; do
        if [ -d "$current/$_PODGRIM_CONFIG_DIR_NAME" ]; then
            _PODGRIM_VAR_project_dir="$current"
            echo "$current"
            return
        fi
        current="$(dirname "$current")"
    done
}

function _PODGRIM_FUNC_config_dir {
    echo "$(_PODGRIM_FUNC_project_dir)/$_PODGRIM_CONFIG_DIR_NAME"
}

function _PODGRIM_FUNC_host_config {
    echo "$(_PODGRIM_FUNC_config_dir)/hosts"
}

function _PODGRIM_FUNC_build_file {
    echo "$(_PODGRIM_FUNC_config_dir)/Containerfile"
}

function _PODGRIM_FUNC_domain {
    if [ -f "$(_PODGRIM_FUNC_config_dir)/domain" ]; then
        cat "$(_PODGRIM_FUNC_config_dir)/domain"
    else
        echo "$_PODGRIM_HOSTNAME_DOMAIN"
    fi
}

function _PODGRIM_FUNC_id {
    local id_file; id_file="$(_PODGRIM_FUNC_config_dir)/id"
    local id

    if [ -f "$id_file" ]; then
        id="$(head -n 1 "$id_file" | cut -d ' ' -f 1)"
    fi

    if [ -z "$id" ]; then
        id="$(basename "$(_PODGRIM_FUNC_project_dir)")"
    fi

    echo "$id"
}

function _PODGRIM_FUNC_hosts {
    grep "^[a-z]" "$(_PODGRIM_FUNC_host_config)" | awk '{print $1}'
}

function _PODGRIM_FUNC_base_image {
    local host="$1"
    grep "^$host\>" "$(_PODGRIM_FUNC_host_config)" | awk '{print $2}'
}

function _PODGRIM_FUNC_image_name {
    local host="$1"
    echo "podgrim-$(_PODGRIM_FUNC_id)-$host-image"
}

function _PODGRIM_FUNC_image_exists {
    local host="$1"
    local image_name; image_name="$(_PODGRIM_FUNC_image_name "$host")"
    podman images | cut -d ' ' -f 1 | grep -q "\<$image_name$"
}

function _PODGRIM_FUNC_container_prefix {
    echo "podgrim-$(_PODGRIM_FUNC_id)-"
}

function _PODGRIM_FUNC_container_name {
    local host="$1"
    echo "$(_PODGRIM_FUNC_container_prefix)$host"
}

function _PODGRIM_FUNC_container_hostname {
    local host="$1"
    echo "${host}.$(_PODGRIM_FUNC_domain)"
}

function _PODGRIM_FUNC_container_ssh_port {
    local host="$1"
    podman port "$(_PODGRIM_FUNC_container_name "$host")" 22 | cut -d ':' -f 2
}

function _PODGRIM_FUNC_container_snapshots {
    local host="$1"
    local container_name
    container_name="$(_PODGRIM_FUNC_container_name "$host")"

    podman images \
            --format="{{.Repository}}:{{.Tag}}#{{.CreatedSince}}" \
        | grep "^localhost/$container_name:snapshot-" \
        | cut -d ':' -f 2- \
        | sort
}

function _PODGRIM_FUNC_container_latest_snapshot {
    local host="$1"
    local container_name
    container_name="$(_PODGRIM_FUNC_container_name "$host")"

    podman images \
            --format="{{.Repository}}:{{.Tag}}" \
        | grep "^localhost/$container_name:snapshot-" \
        | sort \
        | tail -n 1
}

function _PODGRIM_FUNC_containers {
    local state="$1"
    local prefix; prefix="$(_PODGRIM_FUNC_container_prefix)"

    local name
    for name in $(podman ps --all \
        --filter name="$prefix.+" \
        ${state:+ --filter status="$state"} \
        --format="{{.Names}}")
    do
        echo "${name/$prefix}"
    done
}

function _PODGRIM_FUNC_container_status {
    local host; host="$1"
    local status; status="$(podman ps --all \
        --filter "name=^$(_PODGRIM_FUNC_container_name "$host")$" \
        --format="{{.Status}}" \
    )"

    if [ -z "$status" ]; then
        status="Not created"
    fi

    echo "$status"
}

function _PODGRIM_FUNC_ssh_key_dir {
    echo "$(_PODGRIM_FUNC_config_dir)/.ssh"
}

function _PODGRIM_FUNC_private_ssh_key_file {
    echo "$(_PODGRIM_FUNC_ssh_key_dir)/id_rsa"
}

function _PODGRIM_FUNC_public_ssh_key_file {
    echo "$(_PODGRIM_FUNC_private_ssh_key_file).pub"
}

function _PODGRIM_FUNC_generate_ssh_keys {
    local ssh_key_dir; ssh_key_dir="$(_PODGRIM_FUNC_ssh_key_dir)"

    if [ ! -d "$ssh_key_dir" ]; then
        mkdir "$ssh_key_dir"
    fi

    local private_ssh_key
    private_ssh_key="$(_PODGRIM_FUNC_private_ssh_key_file)"

    if [ ! -f "$private_ssh_key" ]; then
        ssh-keygen -q -f "$private_ssh_key" -N '' -C 'podgrim'
    fi

    local public_ssh_key; public_ssh_key="$(_PODGRIM_FUNC_public_ssh_key_file)"
    if [ ! -f "$public_ssh_key" ]; then
        ssh-keygen -y -f "$private_ssh_key" > "$public_ssh_key"
    fi
}


## Commands
function _PODGRIM_CMD_id {
    _PODGRIM_FUNC_id
}

function _PODGRIM_CMD_build {
    local host base_image build_file

    for host in $(_PODGRIM_FUNC_hosts); do
        base_image="$(_PODGRIM_FUNC_base_image "$host")"
        build_file="$(_PODGRIM_FUNC_build_file)"

        if [ -f "$build_file.$host" ]; then
            build_file="$build_file.$host"
        fi

        echo
        echo "Building image for '$host'"

        # TODO: deal with conflicting/shared images names
        podman build \
            --file="$build_file" \
            --from="$base_image" \
            --tag="$(_PODGRIM_FUNC_image_name "$host")" \
            "$(_PODGRIM_FUNC_config_dir)"
    done
}

function _PODGRIM_CMD_halt {
    local host="$1"

    if [ -z "$host" ]; then
        echo "No container specified"
        return 1
    fi

    if [ "$host" = "-a" ] || [ "$host" = "--all" ]; then
        for host in $(_PODGRIM_FUNC_containers running); do
            _PODGRIM_CMD_halt "$host"
        done
        return
    fi

    echo "Halting $host"
    podman stop "$(_PODGRIM_FUNC_container_name "$host")"
}

function _PODGRIM_CMD_destroy {
    local host="$1"

    if [ -z "$host" ]; then
        echo "No container specified"
        return 1
    fi

    if [ "$host" = "-a" ] || [ "$host" = "--all" ]; then
        for host in $(_PODGRIM_FUNC_containers); do
            _PODGRIM_CMD_destroy "$host"
        done
        return
    fi

    # TODO: confirmation prompt

    echo "Destroying $host"
    podman rm -f "$(_PODGRIM_FUNC_container_name "$host")"
}

function _PODGRIM_CMD_snapshot {
    local host="$1"
    local cmd="$2"

    # TODO: check hosts container exists
    local container_name snapshots
    container_name="$(_PODGRIM_FUNC_container_name "$host")"
    snapshots="$(_PODGRIM_FUNC_container_snapshots "$host")"

    case "$cmd" in
        list)
            echo "Snapshots for '$host'"
            echo "$snapshots" \
                | column \
                    --table \
                    --output-separator '    ' \
                    --separator '#' \
                | sed 's/^/    /g'
            echo
        ;;

        pop)
            local pop_snapshot
            pop_snapshot="$(_PODGRIM_FUNC_container_latest_snapshot "$host")"
            echo -n "Removing '$host' snapshot: "
            echo "$pop_snapshot" | cut -d ':' -f 2

            # TODO: confirmation prompt
            podman rmi "$pop_snapshot"
        ;;

        rollback)
            # TODO: Check container exists
            echo -n "Rolling back '$host' to: "
            _PODGRIM_FUNC_container_latest_snapshot "$host" | cut -d ':' -f 2

            # TODO: confirmation prompt
            podman rm -f "$(_PODGRIM_FUNC_container_name "$host")"
            _PODGRIM_CMD_up "$host"
        ;;

        *)
            local snapshot_count
            if [ -z "$snapshots" ]; then
                snapshot_count=1
            else
                snapshot_count="$(( $(echo "$snapshots" | wc -l) + 1 ))"
            fi
            local snapshot_name="$container_name:snapshot-$snapshot_count"

            echo "Creating '$host' snapshot: snapshot-$snapshot_count"
            podman commit \
                "$container_name" \
                "$snapshot_name"
        ;;
    esac
}

function _PODGRIM_CMD_ssh {
    local host="$1"
    shift

    if [ -z "$host" ]; then
        echo "No container specified"
        return 1
    fi

    ssh \
        -q \
        -o UserKnownHostsFile=/dev/null \
        -o StrictHostKeyChecking=no \
        -i "$(_PODGRIM_FUNC_private_ssh_key_file)" \
        -p "$(_PODGRIM_FUNC_container_ssh_port "$host")" \
        root@localhost "$@"
}

function _PODGRIM_CMD_status {
    echo "Current container states:"

    local host
    for host in $(_PODGRIM_FUNC_hosts); do
        echo "$host#$(_PODGRIM_FUNC_container_status "$host")"
    done | column \
        --table \
        --output-separator '    ' \
        --separator '#' \
    | sed 's/^/    /g'
    echo
}

function _PODGRIM_CMD_up {
    local host="$1"

    if [ -z "$host" ]; then
        echo "No container specified"
        return 1
    fi

    if [ "$host" = "-a" ] || [ "$host" = "--all" ]; then
        for host in $(_PODGRIM_FUNC_hosts); do
            _PODGRIM_CMD_up "$host"
        done
        return
    fi

    if ! _PODGRIM_FUNC_image_exists "$host"; then
        echo "No container image for '$host' found"
        return 1
    fi

    if _PODGRIM_FUNC_containers | grep -q "^$host\$"; then
        echo "Starting $host"
        podman start "$(_PODGRIM_FUNC_container_name "$host")"
        return
    fi

    local image_name
    image_name="$(_PODGRIM_FUNC_container_latest_snapshot "$host")"

    if [ -z "$image_name" ]; then
        image_name="$(_PODGRIM_FUNC_image_name "$host")"
    fi

    echo "Creating $host"
    podman run \
        --detach \
        --privileged \
        --name="$(_PODGRIM_FUNC_container_name "$host")" \
        --hostname="$(_PODGRIM_FUNC_container_hostname "$host")" \
        --publish=22 \
        --volume="$(_PODGRIM_FUNC_project_dir)":/podgrim \
        "$image_name" \
        /bin/systemd

    _PODGRIM_FUNC_generate_ssh_keys

    local public_ssh_key
    public_ssh_key="$(cat "$(_PODGRIM_FUNC_public_ssh_key_file)")"

    podman exec \
        "$(_PODGRIM_FUNC_container_name "$host")" \
        /bin/sh -c "
            test -d /root/.ssh || mkdir /root/.ssh
            echo \"$public_ssh_key\" >> /root/.ssh/authorized_keys
        "

    # TODO: post-up scripts
}


## Bash completion commands
function _PODGRIM_CMD_bashcompletion {
    cat <<EOF
        function _PODGRIM_script_completions {
            COMPREPLY=( \$("$(readlink -f "$0")" _bashcompletion "\${COMP_WORDS[@]}") )
        }
        complete -F _PODGRIM_script_completions $(basename "$0")
EOF
}

function _PODGRIM_CMD__bashcompletion {
    shift
    local cmd="$1"
    shift
    local words=( "$@" )

    if [ ${#words[*]} -eq 0 ]; then
        local funcs; funcs="$(declare -F | grep -oP "_PODGRIM_CMD_\K[^_].*")"
        compgen -W "$funcs" "$cmd"
    elif [ ${#words[*]} -ge 1 ]; then
        case "$cmd" in
            up)
                compgen -W "$(_PODGRIM_FUNC_hosts)" "${words[0]}"
            ;;

            halt)
                compgen -W "$(_PODGRIM_FUNC_containers running)" "${words[0]}"
            ;;

            destroy)
                compgen -W "$(_PODGRIM_FUNC_containers)" "${words[0]}"
            ;;

            snapshot)
                if [ ${#words[*]} -eq 1 ]; then
                    compgen -W "$(_PODGRIM_FUNC_containers)" "${words[0]}"
                elif [ ${#words[*]} -eq 2 ]; then
                    compgen -W "list pop rollback" "${words[1]}"
                fi
            ;;

            ssh)
                compgen -W "$(_PODGRIM_FUNC_containers running)" "${words[0]}"
            ;;
        esac
    fi
}


## Main
function _PODGRIM_main {
    local cmd="$1"
    shift
    local func="_PODGRIM_CMD_$cmd"

    if declare -f "$func" > /dev/null ; then
        "$func" "$@"
    else
        echo "Unknown command '$cmd'"
        return 1
    fi
}

_PODGRIM_main "$@"
